import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  count: 0,
};

const getters = {
  // evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd',
};

const mutations = {
  increment() {
    state.count += 1;
  },
  decrement() {
    state.count -= 1;
  },
};

const actions = {
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  incrementIfOdd: ({ commit }) => {
    if ((state.count + 1) % 2 === 0) {
      commit('increment');
    }
  },
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
});
